from flask import Flask, render_template, request, flash, session
from flask_restful import Resource, fields, marshal_with, Api
from models import create_tables, drop_tables, User, Publication
from hashlib import md5
from peewee import IntegrityError
from random import randint
import click
import requests
import datetime

app = Flask(__name__)
app.secret_key = 'bonmatin'

@app.route('/')
def index():
    if not session['username'] and not session['userid']:
        session['username'] = ''
        session['userid'] = ''
    allPublication = Publication.select().order_by(Publication.update_date.desc())
    return render_template('index.html', allPublication = allPublication, loggedUser = session['username'], loggedId = session['userid'])

@app.route('/myProfil')
def myProfil():
    allPublication = Publication.select().where(Publication.user == session['userid']).order_by(Publication.update_date.desc())
    return render_template('index.html', allPublication = allPublication, loggedUser = session['username'], loggedId = session['userid'])

#Création d'un compte
@app.route('/register')
def inscription():
    return render_template('inscription.html', loggedUser = session['username'], loggedId = session['userid'])

@app.route('/register', methods=['POST'])
def registerPost():
    message = ''
    newEmail = request.form.get('email')
    newFirstName = request.form.get('firstname')
    newLastName = request.form.get('lastname')
    newUsername = request.form.get('username')
    newPassword = md5(request.form.get('password').encode('utf-8')).hexdigest()
    try:
        User.create(
            username = newUsername,
            first_name  = newFirstName,
            last_name = newLastName,
            email = newEmail,
            password = newPassword
        )
    except IntegrityError as message:
        return render_template('inscription.html', error = message)

    return render_template('connexion.html', error = message)

#Connection
@app.route('/login')
def login():
    return render_template('connexion.html', loggedUser = session['username'])

@app.route('/login', methods=['POST'])
def loginPost():
    emailLogin = request.form.get('email')
    passwordLogin = md5(request.form.get('password').encode('utf-8')).hexdigest()
    query = User.select().where((User.email == emailLogin) & (User.password == passwordLogin))
    for user in query:
        if user.username:
            session['username'] = user.username
            session['userid'] = user.id

    return index()

#Deconnection
@app.route('/logout')
def logout():
    session['username'] = ''
    session['userid'] = ''
    return index()

#Création d'un post
@app.route('/create')
def creation():
    return render_template('createpage.html')

@app.route('/create', methods=['POST'])
def creationPost():
    actualTime = datetime.datetime.now()
    title = request.form.get('title')
    body = request.form.get('body')
    update_date = actualTime
    creation_date = actualTime
    Publication.create(
        title = title,
        body = body,
        update_date = update_date,
        creation_date = creation_date,
        user = session['userid']
    )

    return index()

#Modification d'un post
@app.route('/edit', methods=['POST'])
def edit():
    publicationId = request.form.get('publicationid')
    selectedPublication = Publication.select().where(Publication.id == publicationId)
    return render_template('modificationpage.html', selectedPublication = selectedPublication, loggedUser = session['username'], loggedId = session['userid'])

@app.route('/edited', methods=['POST'])
def edited():
    titre = request.form.get('title')
    body = request.form.get('body')
    publicationId = request.form.get('publicationid')
    action = request.form.get("action")

    if (action == "delete"):
        #Supression d'un post
        query = Publication.delete().where(Publication.id == publicationId)
    else:
        query = Publication.update(title = titre, body = body, update_date = datetime.datetime.now()).where(Publication.id == publicationId)

    query.execute()
    return index()


@app.cli.command()
def initdb():
    create_tables()
    click.echo('Database created')


@app.cli.command()
def dropdb():
    drop_tables()
    click.echo('Database dropped')

@app.cli.command()
def fakedata():
    from faker import Faker
    fake = Faker()
    for pk in range(0, 42):
        User.create(
            username = fake.user_name(),
            first_name  = fake.first_name(),
            last_name = fake.last_name(),
            email = fake.email(),
            password = md5('password'.encode('utf-8')).hexdigest()
        )
        Publication.create(
            title = fake.first_name(),
            body  = fake.text(),
            creation_date = fake.date(),
            update_date = fake.date(),
            user = randint(1, 42)
        )