import datetime
from peewee import (
    SqliteDatabase, 
    Model,
    CharField,
    DateField,
    DateTimeField,
    FloatField,
    ForeignKeyField,
)

database = SqliteDatabase('data.sqlite3')

class BaseModel(Model):
    
    class Meta:
        database = database


class User(BaseModel):
    username = CharField(max_length=255, unique=True)
    first_name  = CharField(max_length=255)
    last_name = CharField(max_length=255)
    email = CharField(max_length=255, unique=True)
    password = CharField(max_length=255)

class Publication(BaseModel):
    title = CharField(max_length=255)
    body  = CharField(max_length=255)
    creation_date = DateTimeField()
    update_date = DateTimeField()
    user = ForeignKeyField(User, backref='userPublication')

def create_tables():
    with database:
        database.create_tables([Publication])
        database.create_tables([User])


def drop_tables():
    with database:
        database.drop_tables([Publication])
        database.drop_tables([User])